import 'package:http/http.dart' as http;
import 'dart:convert';
import '../model/login.dart';

class APIService {
  Future<LoginResponseModel> login(LoginRequestModel requestModel) async {
    var url = requestModel.role_index == 0
        ? 'http://10.0.2.2:3000/api/student_login'
        : 'http://10.0.2.2:3000/api/librarian_login';
    // var url = 'https://reqres.in/api/login';
    // http.Response response = await http.get(Uri.parse(url));

    http.Response response =
        await http.post(Uri.parse(url), body: requestModel.toJson());
    if (response.statusCode == 200 || response.statusCode == 400) {
      // var jsonData = await jsonDecode(response.body);
      // print(requestModel.toJson());
      // print(json.decode(response.body));
      return LoginResponseModel.fromJson(json.decode(response.body));
      // jsonDecode(response.body),
      // jsonData);
    } else {
      throw Exception('Failed to load data!');
    }
  }
}
