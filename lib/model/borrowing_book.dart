// ignore_for_file: non_constant_identifier_names

class BorrowingBook {
  final int id;
  final int id_book;
  final String title;
  final String borrow_start;
  final String borrow_end;
  final String img;

  const BorrowingBook({
    required this.id,
    required this.id_book,
    required this.title,
    required this.borrow_start,
    required this.borrow_end,
    required this.img,
  });

  factory BorrowingBook.fromJson(Map<String, dynamic> json) => BorrowingBook(
        id: json['id'],
        id_book: json['id_book'],
        title: json['title'],
        borrow_start: json['borrow_start'],
        borrow_end: json['borrow_end'],
        img: json['img'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'id_book': id_book,
        'title': title,
        'borrow_start': borrow_start,
        'borrow_end': borrow_end,
        'img': img,
      };
}
