class LoginResponseModel {
  final String name;
  final String role;
  final String token;
  final String error;

  LoginResponseModel(
      {required this.name,
      required this.role,
      required this.token,
      required this.error});

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) {
    return LoginResponseModel(
      name: json["name"] ?? "",
      role: json["role"] ?? "",
      token: json["token"] ?? "",
      error: json["error"] ?? "",
    );
  }
}

class LoginRequestModel {
  String email;
  String password;
  int role_index;

  LoginRequestModel({
    required this.email,
    required this.password,
    required this.role_index,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'email': email.trim(),
      'password': password.trim(),
    };

    return map;
  }
}
