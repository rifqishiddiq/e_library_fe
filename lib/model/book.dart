// ignore_for_file: non_constant_identifier_names

class Book {
  final int id;
  final String title;
  final String author;
  final String publisher;
  final String synopsis;
  final bool status_available;
  final String img;

  const Book({
    required this.id,
    required this.author,
    required this.title,
    required this.publisher,
    required this.synopsis,
    required this.status_available,
    required this.img,
  });

  factory Book.fromJson(Map<String, dynamic> json) => Book(
        id: json['id'],
        author: json['author'],
        title: json['title'],
        publisher: json['publisher'],
        synopsis: json['synopsis'],
        status_available: json['status_available'],
        img: json['img'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'author': author,
        'publisher': publisher,
        'synopsis': synopsis,
        'status_available': status_available,
        'img': img,
      };
}
