// ignore_for_file: non_constant_identifier_names

class ListBorrowing {
  // final int id;
  final int id_book;
  final String title;
  final int id_student;
  final String name;
  final String borrow_start;
  // final String borrow_end;
  final String img;

  const ListBorrowing({
    // required this.id,
    required this.id_book,
    required this.title,
    required this.id_student,
    required this.name,
    required this.borrow_start,
    // required this.borrow_end,
    required this.img,
  });

  factory ListBorrowing.fromJson(Map<String, dynamic> json) => ListBorrowing(
        // id: json['id'],
        id_book: json['id_book'],
        title: json['title'],
        id_student: json['id_student'],
        name: json['name'],
        borrow_start: json['borrow_start'],
        // borrow_end: json['borrow_end'],
        img: json['img'],
      );

  Map<String, dynamic> toJson() => {
        // 'id': id,
        'id_book': id_book,
        'title': title,
        'id_student': id_student,
        'name': name,
        'borrow_start': borrow_start,
        // 'borrow_end': borrow_end,
        'img': img,
      };
}
