import 'package:e_library_itats/model/borrowing_book.dart';
// import 'package:e_library_itats/data/borrowing_book.dart';
import 'package:flutter/material.dart';

class Borrowing extends StatefulWidget {
  final List<BorrowingBook> allBorrowingBook;

  const Borrowing({required this.allBorrowingBook});

  @override
  BorrowingState createState() => BorrowingState();
}

class BorrowingState extends State<Borrowing> {
  late List<BorrowingBook> books;
  String query = '';

  @override
  void initState() {
    super.initState();

    books = widget.allBorrowingBook;
    print(widget.allBorrowingBook.length);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemCount: books.length,
                itemBuilder: (context, index) {
                  final book = books[index];

                  return buildBook(book);
                },
              ),
            ),
          ],
        ),
      );

  Widget buildBook(BorrowingBook book) => GestureDetector(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  Image.network(
                    book.img,
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(left: 10)),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
                const Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
                Text(
                  book.title,
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 16.0,
                  ),
                ),
                const Padding(padding: EdgeInsets.symmetric(vertical: 10.0)),
                Text(
                  'borrow date: ' + book.borrow_start,
                  style: const TextStyle(fontSize: 14.0, color: Colors.black54),
                ),
                const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
                Text(
                  'return date: ' + book.borrow_end,
                  style: const TextStyle(fontSize: 14.0, color: Colors.black54),
                ),
              ]),
            ],
          ),
        ),
      );
}
