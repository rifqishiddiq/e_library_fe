// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:e_library_itats/pages/history_borrowing_student/borrowed.dart';
import 'package:e_library_itats/pages/history_borrowing_student/borrowing.dart';
import 'package:flutter/material.dart';
// import 'package:e_library_itats/pages/detail.dart';
import 'package:e_library_itats/widget/navigation_drawer.dart';
import 'package:e_library_itats/model/borrowing_book.dart';
import 'package:e_library_itats/data/borrowing_book.dart';

class HistoryBorrowingStudent extends StatefulWidget {
  const HistoryBorrowingStudent({Key? key}) : super(key: key);

  @override
  _HistoryBorrowingStudentState createState() =>
      _HistoryBorrowingStudentState();
}

class _HistoryBorrowingStudentState extends State<HistoryBorrowingStudent> {
  int index = 0;

  late List<BorrowingBook> books;
  String query = '';

  @override
  void initState() {
    super.initState();

    books = allBorrowingBook;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: NavigationDrawer(),
        appBar: AppBar(
          title: const Text('History Pinjam'),
          centerTitle: true,
          bottom: TabBar(
            onTap: (value) {
              setState(() => index = value);
            },
            tabs: [
              Tab(
                text: "Borrowing",
              ),
              Tab(
                text: "Borrowed",
              ),
            ],
          ),
        ),
        body: buildPages(),
      ),
    );
  }

  Widget buildPages() {
    switch (index) {
      case 0:
        return Borrowing(
            allBorrowingBook: allBorrowingBook.where((book) {
          final titleLower = book.borrow_end;

          return titleLower.isEmpty;
        }).toList());
      case 1:
        return Borrowed(
            allBorrowingBook: allBorrowingBook.where((book) {
          final titleLower = book.borrow_end;

          return titleLower.isNotEmpty;
        }).toList());
      default:
        return Container();
    }
  }
}
