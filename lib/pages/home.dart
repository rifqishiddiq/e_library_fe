// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:e_library_itats/pages/detail.dart';
import 'package:e_library_itats/widget/search_widget.dart';
import 'package:e_library_itats/widget/navigation_drawer.dart';
import 'package:e_library_itats/model/book.dart';
import 'package:e_library_itats/data/book.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late List<Book> books;
  String query = '';

  @override
  void initState() {
    super.initState();

    books = allBooks;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        title: const Text('List buku'),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          buildSearch(),
          Expanded(
            child: ListView.builder(
              itemCount: books.length,
              itemBuilder: (context, index) {
                final book = books[index];

                return buildBook(book);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Title or Author Name',
        onChanged: searchBook,
      );

  // Widget buildBook(Book book) => ListTile(
  //       onTap: () => {
  //         Navigator.push(
  //           context,
  //           MaterialPageRoute(
  //             builder: (context) => DetailScreen(book: book),
  //           ),
  //         )
  //         // Navigator.pushNamed(context, '/detail', arguments: {'book': book})
  //       },
  //       leading: Image.network(
  //         book.img,
  //         fit: BoxFit.cover,
  //         width: 50,
  //         height: 50,
  //       ),
  //       title: Text(book.title),
  //       subtitle: Text(book.author),
  //     );

  Widget buildBook(Book book) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailScreen(book: book),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  Image.network(
                    book.img,
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(left: 10)),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
                    Text(
                      book.title,
                      style: const TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16.0,
                      ),
                    ),
                    const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
                    Text(
                      book.author,
                      style: const TextStyle(
                          fontSize: 14.0, color: Colors.black54),
                    ),
                    const Padding(padding: EdgeInsets.symmetric(vertical: 8.0)),
                    book.status_available
                        ? Text(
                            'available',
                            style: const TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.green),
                          )
                        : Text(
                            'not available',
                            style: const TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.red),
                          ),
                  ]),
            ],
          ),
        ),
      );

  void searchBook(String query) {
    final books = allBooks.where((book) {
      final titleLower = book.title.toLowerCase();
      final authorLower = book.author.toLowerCase();
      final searchLower = query.toLowerCase();

      return titleLower.contains(searchLower) ||
          authorLower.contains(searchLower);
    }).toList();
    print(books);

    setState(() {
      this.query = query;
      this.books = books;
    });
  }
}
