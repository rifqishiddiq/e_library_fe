// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:e_library_itats/pages/detail.dart';
import 'package:e_library_itats/widget/navigation_drawer.dart';
import 'package:e_library_itats/model/list_borrowing.dart';
import 'package:e_library_itats/data/list_borrowing.dart';

class ListBorrowingBook extends StatefulWidget {
  const ListBorrowingBook({Key? key}) : super(key: key);

  @override
  _ListBorrowingBookState createState() => _ListBorrowingBookState();
}

class _ListBorrowingBookState extends State<ListBorrowingBook> {
  late List<ListBorrowing> books;
  String query = '';

  @override
  void initState() {
    super.initState();

    books = allBorrowing;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: NavigationDrawer(),
        appBar: AppBar(
          title: const Text('List Peminjaman'),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            // buildSearch(),
            Expanded(
              child: ListView.builder(
                itemCount: books.length,
                itemBuilder: (context, index) {
                  final book = books[index];

                  return buildBook(book);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBook(ListBorrowing book) => GestureDetector(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  Image.network(
                    book.img,
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(left: 10)),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
                const Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
                Text(
                  book.title,
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 16.0,
                  ),
                ),
                const Padding(padding: EdgeInsets.symmetric(vertical: 10.0)),
                Text(
                  'borrowed by: ' + book.name,
                  style: const TextStyle(fontSize: 16.0, color: Colors.black54),
                ),
                const Padding(padding: EdgeInsets.symmetric(vertical: 10.0)),
                Text(
                  'borrow date: ' + book.borrow_start,
                  style: const TextStyle(fontSize: 14.0, color: Colors.black54),
                ),
                // const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
                // Text(
                //   'return date: ' + book.borrow_end,
                //   style: const TextStyle(fontSize: 14.0, color: Colors.black54),
                // ),
              ]),
            ],
          ),
        ),
      );
}
