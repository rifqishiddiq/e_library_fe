// ignore_for_file: prefer_const_constructors, override_on_non_overriding_member, deprecated_member_use, await_only_futures, prefer_const_literals_to_create_immutables, must_call_super

import 'package:flutter/material.dart';
// import 'package:e_library_itats/Screens/Login/components/background.dart';
// import 'package:e_library_itats/Screens/Signup/signup_screen.dart';
// import 'package:e_library_itats/components/already_have_an_account_acheck.dart';
import 'package:e_library_itats/api/api_service.dart';
import 'package:e_library_itats/components/rounded_button.dart';
// import 'package:e_library_itats/components/rounded_input_field.dart';
// import 'package:e_library_itats/components/rounded_password_field.dart';
import 'package:e_library_itats/components/text_field_container.dart';
import 'package:e_library_itats/model/login.dart';
import 'package:e_library_itats/constants.dart';
// import 'package:e_library_itats/widget/ProgressHUD.dart';
import 'package:e_library_itats/widget/shared_preference.dart';
// import 'package:flutter_svg/svg.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late LoginRequestModel loginRequestModel;
  bool _hiddenPassword = true;
  bool _validatorEmail = true;
  bool isApiCallProcess = false;
  // bool _isStudent = true;
  int index = 0;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  // void loginRole(bool role) {
  //   setState(() {
  //     _isStudent = role;
  //   });
  // }

  void showPassword(bool pass) {
    setState(() {
      _hiddenPassword = (pass ? false : true);
    });
  }

  bool checkEmail(String email, bool pass) {
    setState(() {
      _validatorEmail = (email.contains('@') ? true : false);
    });
    return _validatorEmail;
  }

  @override
  void initState() {
    super.initState();
    loginRequestModel =
        LoginRequestModel(email: '', password: '', role_index: index);
  }

  // @override
  // Widget build(BuildContext context) {
  //   return ProgressHUD(
  //     child: _uiSetup(context),
  //     bottomNavigationBar: buildBottomBar(),
  //     inAsyncCall: isApiCallProcess,
  //     opacity: 0.3,
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
      body: SizedBox(
        width: double.infinity,
        height: size.height,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            SingleChildScrollView(
              child: Form(
                key: globalFormKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      "LOGIN",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: size.height * 0.03),
                    SizedBox(height: size.height * 0.03),
                    TextFieldContainer(
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (input) => loginRequestModel.email = input!,
                        // validator: (input) => !input!.contains('@')
                        //     ? "Email Id should be valid"
                        //     : null,
                        onChanged: (value) {},
                        cursorColor: kPrimaryColor,
                        decoration: InputDecoration(
                          hintText: "Email",
                          icon: Icon(
                            Icons.person,
                            color: kPrimaryColor,
                          ),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    !_validatorEmail
                        ? Text(
                            'Email format should be valid',
                            style: TextStyle(color: Colors.red),
                          )
                        : SizedBox.shrink(),
                    TextFieldContainer(
                      child: TextFormField(
                        obscureText: _hiddenPassword,
                        onSaved: (input) => loginRequestModel.password = input!,
                        onChanged: (value) {},
                        cursorColor: kPrimaryColor,
                        decoration: InputDecoration(
                          hintText: "Password",
                          icon: Icon(
                            Icons.lock,
                            color: kPrimaryColor,
                          ),
                          suffixIcon: IconButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              icon: Icon(
                                (_hiddenPassword
                                    ? Icons.visibility_off
                                    : Icons.visibility),
                                color: kPrimaryColor,
                              ),
                              onPressed: () => showPassword(_hiddenPassword)),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    // RoundedInputField(
                    //   onChanged: (value) {},
                    // ),
                    // RoundedPasswordField(
                    //   onChanged: (value) {},
                    // ),
                    RoundedButton(
                      text: "LOGIN",
                      press: () => processLogin(),
                      // press: () async {
                      //   // SharedPrefrence().setRole(('student').toString());
                      //   // SharedPrefrence().setName(('student').toString());
                      //   // SharedPrefrence().setRole(('librarian').toString());
                      //   // SharedPrefrence().setName(('librarian').toString());
                      //   // Navigator.pushReplacementNamed(context, '/home');
                      // },
                    ),
                    SizedBox(height: size.height * 0.03),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: buildBottomBar(),
    );
  }

  Widget buildBottomBar() {
    final styleBig = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);
    final stylesmall = TextStyle(fontSize: 13, color: Colors.white70);
    return BottomNavigationBar(
        backgroundColor: Theme.of(context).primaryColor,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white70,
        // selectedIconTheme: IconThemeData(opacity: 0.0, size: 0),
        // unselectedIconTheme: IconThemeData(opacity: 0.0, size: 0),
        currentIndex: index,
        items: [
          BottomNavigationBarItem(
            icon: Text(
              'Login as:',
              style: stylesmall,
            ),
            title: Text(
              'Student',
              style: styleBig,
            ),
          ),
          BottomNavigationBarItem(
            icon: Text('Login as:', style: stylesmall),
            title: Text(
              'Librarian',
              style: styleBig,
            ),
          ),
        ],
        onTap: (int index) => setState(() => this.index = index));
  }

  processLogin() async {
    if (validateAndSave()) {
      // print(loginRequestModel.toJson());
      loginRequestModel.role_index = index;
      var test = await checkEmail(loginRequestModel.email, _validatorEmail);
      if (test &&
          loginRequestModel.password != null &&
          loginRequestModel.password != '') {
        setState(() {
          isApiCallProcess = true;
        });

        BuildContext? dialogContext;
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            dialogContext = context;
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        );
        APIService apiService = APIService();
        apiService.login(loginRequestModel).then((value) async {
          setState(() {
            isApiCallProcess = false;
          });

          await Future.delayed(const Duration(milliseconds: 2000), () {});
          Navigator.pop(dialogContext!);
          if (value.token.isNotEmpty) {
            final snackBar = SnackBar(content: Text("Login Successful"));
            scaffoldKey.currentState!.showSnackBar(snackBar);

            SharedPrefrence().setName((value.name).toString());
            if (index == 1) {
              SharedPrefrence().setRole(('librarian').toString());
            } else {
              SharedPrefrence().setRole(('student').toString());
            }
            SharedPrefrence().setToken((value.token).toString());
            // SharedPrefrence().setName((value.email).toString());
            // SharedPrefrence().setUserId((value.id).toString());

            ///This is used when user loged in you can set this true,
            ///next time you open you need to check loginc in main.dart or splashscreen if this is true if it is true then
            ///redirect to home page it is false then redirect to Login page
            ///When you logout the app make sure you set this as false like "SharedPrefrence().setLoggedIn(false);"
            SharedPrefrence().setLoggedIn(true);

            Navigator.pushReplacementNamed(context, '/home');
          } else {
            final snackBar = SnackBar(content: Text(value.error));
            scaffoldKey.currentState!.showSnackBar(snackBar);
          }
        });
      }
    }
  }

  buildShowDialog() {
    // _NavigateLogout(context);
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  @override
  bool get wantKeepAlive => throw UnimplementedError();
}
