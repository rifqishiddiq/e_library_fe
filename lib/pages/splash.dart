// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:e_library_itats/widget/shared_preference.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _NavigateToHome();
  }

  _NavigateToHome() async {
    await Future.delayed(const Duration(milliseconds: 2000), () {});
    Future status_login = SharedPrefrence().getLogedIn();
    status_login.then((data) async {
      data
          ? Navigator.pushReplacementNamed(context, '/home')
          : Navigator.pushReplacementNamed(context, '/login');
    });
    // if (SharedPrefrence().getLogedIn() as bool) {
    //   Navigator.pushReplacementNamed(context, '/home');
    // } else {
    //   Navigator.pushReplacementNamed(context, '/login');
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: const Text('Splash Screen'),
        ),
      ),
    );
  }
}
