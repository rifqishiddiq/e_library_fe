import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BookMain extends StatefulWidget {
  const BookMain({Key? key}) : super(key: key);

  @override
  State<BookMain> createState() => _BookMainState();
}

class _BookMainState extends State<BookMain> {
  fetchCategory() async {
    var url = 'http://10.0.2.2:3000/api/categories';
    http.Response response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var jsonData = await jsonDecode(response.body);
      print(jsonData);
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchCategory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.lightBlue,
        title: Text('Test Page'),
      ),
      body: SafeArea(child: Text('ini page test')),
    );
  }
}
