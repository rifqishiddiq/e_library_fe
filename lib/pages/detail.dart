// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:e_library_itats/model/book.dart';
import 'package:e_library_itats/widget/navigation_drawer.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key, required this.book}) : super(key: key);

  final Book book;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavigationDrawer(),
        appBar: AppBar(
          title: const Text('Detail buku'),
          centerTitle: true,
        ),
        body: SafeArea(
            child: Stack(children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: Image.network(
                    book.img,
                    // fit: BoxFit.cover,
                    // width: 100,
                    height: 150,
                  ),
                ),
                Divider(
                  height: 20.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          book.title,
                          style: TextStyle(
                            // color: Colors.amberAccent[200],
                            // fontWeight: FontWeight.bold,
                            fontSize: 24.0,
                            // letterSpacing: 2.0,
                          ),
                        ),
                        SizedBox(height: 4.0),
                        Text(
                          book.author,
                          style: TextStyle(
                            color: Colors.black87,
                            // fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                            // letterSpacing: 2.0,
                          ),
                        ),
                      ],
                    ),
                    book.status_available
                        ? Text(
                            'available',
                            style: const TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.green),
                          )
                        : Text(
                            'not available',
                            style: const TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.red),
                          ),
                  ],
                ),
                SizedBox(height: 20.0),
                Text(
                  'Synopsis',
                  style: TextStyle(
                    color: Colors.black54,
                    // fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                    // letterSpacing: 2.0,
                  ),
                ),
                SizedBox(height: 6.0),
                Text(
                  book.synopsis,
                  style: TextStyle(
                    color: Colors.black,
                    // fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                    // letterSpacing: 2.0,
                  ),
                ),
              ],
            ),
          ),
          IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ])));
  }
}
