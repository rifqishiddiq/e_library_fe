// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:e_library_itats/components/text_field_container.dart';
import 'package:e_library_itats/constants.dart';

class RoundedPasswordField extends StatefulWidget {
  final ValueChanged<String> onChanged;
  const RoundedPasswordField({
    required this.onChanged,
  });

  @override
  State<RoundedPasswordField> createState() => _RoundedPasswordFieldState();
}

class _RoundedPasswordFieldState extends State<RoundedPasswordField> {
  bool _hiddenPassword = true;

  void showPassword(bool pass) {
    setState(() {
      _hiddenPassword = (pass ? false : true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        obscureText: _hiddenPassword,
        onChanged: widget.onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          hintText: "Password",
          icon: Icon(
            Icons.lock,
            color: kPrimaryColor,
          ),
          suffixIcon: IconButton(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              icon: Icon(
                (_hiddenPassword ? Icons.visibility_off : Icons.visibility),
                color: kPrimaryColor,
              ),
              onPressed: () => showPassword(_hiddenPassword)),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
