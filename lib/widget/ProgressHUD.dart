// ignore_for_file: deprecated_member_use, prefer_collection_literals, prefer_const_constructors

import 'package:flutter/material.dart';

class ProgressHUD extends StatelessWidget {
  final Widget child;
  final Widget bottomNavigationBar;
  final bool inAsyncCall;
  final double opacity;
  final Color color;
  // final Animation<Color> valueColor;

  ProgressHUD({
    required this.child,
    required this.bottomNavigationBar,
    required this.inAsyncCall,
    this.opacity = 0.3,
    this.color = Colors.black,
    // required this.valueColor,
  });

  @override
  Widget build(BuildContext context) {
    List<Widget> widgetList = <Widget>[];
    widgetList.add(child);
    widgetList.add(bottomNavigationBar);
    // if (inAsyncCall) {
    //   final modal = Stack(
    //     children: [
    //       Opacity(
    //         opacity: opacity,
    //         child: ModalBarrier(dismissible: false, color: color),
    //       ),
    //       Center(child: CircularProgressIndicator()),
    //     ],
    //   );
    //   widgetList.add(modal);
    // }
    return Stack(
      children: widgetList,
    );
  }
}
