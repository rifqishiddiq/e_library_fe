// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:e_library_itats/widget/shared_preference.dart';

class NavigationDrawer extends StatefulWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  State<NavigationDrawer> createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  String _name = '';
  String _role = '';

  _getPreference() async {
    Future pref_name = SharedPrefrence().getName();
    pref_name.then((data) async {
      setState(() {
        _name = data;
      });
    });
    Future pref_role = SharedPrefrence().getRole();
    pref_role.then((data) async {
      setState(() {
        _role = data;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getPreference();
  }

  _NavigateLogout(context) async {
    SharedPrefrence().setToken(('').toString());
    SharedPrefrence().setUserId(('').toString());
    SharedPrefrence().setName(('').toString());
    SharedPrefrence().setLoggedIn(false);

    await Future.delayed(const Duration(milliseconds: 2000), () {});
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    // final padding = EdgeInsets.symmetric(horizontal: 20);
    return Drawer(
      child: Material(
        color: Colors.blue,
        child: ListView(
          // padding: padding,
          children: [
            const SizedBox(
              height: 34,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: Text(
                'Hi, $_name',
                style: const TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(
              height: 24,
            ),
            buildMenuItem(
                text: 'List Buku',
                icon: Icons.list,
                onClicked: () => selectedItem(context, 'book-list')),
            _role == 'student'
                ? buildMenuItem(
                    text: 'History Pinjam',
                    icon: Icons.history,
                    onClicked: () =>
                        selectedItem(context, 'history-borrowing-book'))
                : buildMenuItem(
                    text: 'List Peminjaman',
                    icon: Icons.history,
                    onClicked: () => selectedItem(context, 'list-borrowing')),
            buildMenuItem(
                text: 'Logout',
                icon: Icons.logout,
                // onClicked: () => selectedItem(context, 'logout')),
                onClicked: () => buildShowDialog(context)),
          ],
        ),
      ),
    );
  }

  buildMenuItem(
      {required String text, required IconData icon, VoidCallback? onClicked}) {
    const color = Colors.white;
    const hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(
        icon,
        color: color,
        size: 32,
      ),
      title: Text(text, style: const TextStyle(color: color, fontSize: 18)),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  selectedItem(BuildContext context, String menu) {
    switch (menu) {
      case 'book-list':
        Navigator.pushReplacementNamed(context, '/home');
        break;
      case 'history-borrowing-book':
        Navigator.pushReplacementNamed(context, '/history_borrowing_student');
        break;
      case 'list-borrowing':
        Navigator.pushReplacementNamed(context, '/list_borrowing');
        break;
      // case 'logout':
      //   _NavigateLogout(context);
      //   break;
      default:
    }
  }

  buildShowDialog(BuildContext context) {
    _NavigateLogout(context);
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}
