// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:e_library_itats/pages/login.dart';
import 'package:e_library_itats/pages/splash.dart';
import 'package:e_library_itats/pages/home.dart';
import 'package:e_library_itats/pages/history_borrowing_student/main.dart';
import 'package:e_library_itats/pages/list_borrowing.dart';

void main() => runApp(MaterialApp(
      routes: {
        '/': (context) => SplashScreen(),
        '/login': (context) => Login(), // list book
        '/home': (context) => Home(), // list book
        '/history_borrowing_student': (context) =>
            HistoryBorrowingStudent(), // list book
        '/list_borrowing': (context) => ListBorrowingBook(), // list book
        // '/detail': (context) => DetailScreen()
      },
    ));
