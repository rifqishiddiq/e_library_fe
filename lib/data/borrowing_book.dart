// ignore_for_file: prefer_const_constructors

import 'package:e_library_itats/model/borrowing_book.dart';

final allBorrowingBook = <BorrowingBook>[
  BorrowingBook(
    id: 1,
    id_book: 10,
    title: 'Butterflies',
    borrow_start: '2022-01-11',
    borrow_end: '',
    img:
        'https://images.unsplash.com/photo-1615300236079-4bdb43bd9a9a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80',
  ),
  BorrowingBook(
    id: 2,
    id_book: 5,
    title: 'Parkour',
    borrow_start: '2022-01-09',
    borrow_end: '2022-01-12',
    img:
        'https://images.unsplash.com/photo-1615286505008-cbca9896192f?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=962&q=80',
  ),
  BorrowingBook(
    id: 3,
    id_book: 2,
    title: 'Busy City Life',
    borrow_start: '2022-01-04',
    borrow_end: '2022-01-10',
    img:
        'https://images.unsplash.com/photo-1615346340977-cf7f5a8f3059?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
  ),
];
