// ignore_for_file: prefer_const_constructors

import 'package:e_library_itats/model/list_borrowing.dart';

final allBorrowing = <ListBorrowing>[
  ListBorrowing(
    id_book: 10,
    title: 'Butterflies',
    id_student: 1,
    name: 'Student 1',
    borrow_start: '2022-01-11',
    img:
        'https://images.unsplash.com/photo-1615300236079-4bdb43bd9a9a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80',
  ),
  ListBorrowing(
    id_book: 5,
    title: 'Parkour',
    id_student: 2,
    name: 'Student 2',
    borrow_start: '2022-01-09',
    img:
        'https://images.unsplash.com/photo-1615286505008-cbca9896192f?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=962&q=80',
  ),
  ListBorrowing(
    id_book: 2,
    title: 'Busy City Life',
    id_student: 1,
    name: 'Student 1',
    borrow_start: '2022-01-06',
    img:
        'https://images.unsplash.com/photo-1615346340977-cf7f5a8f3059?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
  ),
  ListBorrowing(
    id_book: 7,
    title: 'Beahces',
    id_student: 2,
    name: 'Student 2',
    borrow_start: '2022-01-04',
    img:
        'https://images.unsplash.com/photo-1615357633073-a7b67638dedb?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=564&q=80',
  ),
  ListBorrowing(
    id_book: 9,
    title: 'Magnificent Forests',
    id_student: 3,
    name: 'Student 3',
    borrow_start: '2022-01-02',
    img:
        'https://images.unsplash.com/photo-1615331224984-281512856592?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
  ),
];
